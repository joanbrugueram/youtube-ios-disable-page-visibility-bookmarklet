# Bookmarklet to disable Page Visibility API in YouTube for Safari (iOS)  #

## What is this?

The [Page Visibility API](https://www.w3.org/TR/page-visibility/), which is intended to determine save power and CPU usage, is used by the [mobile YouTube website](https://m.youtube.com).

While I'm not sure when it was implemented by either the mobile YouTube website or Safari for iOS, it recently started being enforced, making it impossible to keep a video running while on another tab or app, sparkling user complaints like [this one](https://discussions.apple.com/thread/7734093).

While there appear to be various workarounds (private mode, desktop site, ad-ridden applications, alternative sites, video downloaders like [youtube-dl](https://rg3.github.io/youtube-dl/), etc.), nothing seems to be as convenient and effective as returning to the old behaviour.

## So what can I do about it?

[Create a bookmarklet](https://apple.stackexchange.com/questions/74195/add-bookmarklet-to-mobile-safari) in Safari for iOS with the following contents:

```javascript
javascript:document.__defineGetter__("visibilityState", function() {return 'visible';} );
```

You should execute this bookmark while on the YouTube tab (unfortunately, it isn't permanent, so you need to reapply it on each page reload). No Jailbreak or extra apps are needed. You can then switch to another tab or app and the video will not stop playing.

## How does it work?

It monkey-patches the portion of the Page Visibility API used by the mobile YouTube website to check if the page is visible, making it always think the page is visible.

This bookmarklet may also work for other sites with similar behaviour. It doesn't patch the entire Page Visibility API (e.g. `document.hidden`), but it's good enough for the mobile YouTube website, and is a start to a more general solution. If you have some time, feel free to make a better version.

## License gibberish

I doubt such a short and obvious piece of code could be copyrightable, but in case of doubt, consider it under the [WTFPL](http://www.wtfpl.net/txt/copying/).